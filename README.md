<h1 align="center">Welcome to Library 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.0-blue.svg?cacheSeconds=2592000" />
</p>

> API for get list of books, submit an order and listing order

### ✨ [Demo](http://localhost:3000)

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```

## API Usage
```sh
Get List Books
[GET]http://localhost:3000/getBookList/?keyword=animal
```
```sh
Order Book
[POST]http://localhost:3000/orderBook
- bookId
- pickUpDtm
```
```sh
Get Order 
[GET]http://localhost:3000/orderList?bookId=d5298d3685200c741556ea26bc052ec3
```

## Author

👤 **Eko Prasetyo**

