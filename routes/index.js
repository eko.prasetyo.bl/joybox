var express = require('express');
var router = express.Router();
var Books = require("../controllers/Books");
var Order = require("../controllers/Order");

router.get('/getBookList', new Books().findBook);
router.post('/orderBook', new Order().submitOrder);
router.get('/orderList', new Order().listOrder);

module.exports = router;
