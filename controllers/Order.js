const md5 = require('md5');
class Order {
    async submitOrder(req, res){
        let bodyRequest = req.body;

        if("bookId" in bodyRequest && "pickUpDtm" in bodyRequest){
            if(bodyRequest.bookId in global.books){
                let orders = [];
                if(bodyRequest.bookId in global.orders){
                    orders = global.orders[bodyRequest.bookId];
                }
                orders.push(bodyRequest.pickUpDtm);
                global.orders[bodyRequest.bookId] = orders;
                res.json({"status" : "Success"});
            }else{
                res.json({"error" : "Book not available"});
            }
        }else{
            res.json({"error" : "Invalid parameter bookId or pickUpDtm"});
        }
    }

    async listOrder(req, res){
        let params = req.query;
        if("bookId" in params) {
            if(params.bookId in global.orders){
                res.json({"status" : "Success", "data" : global.orders[params.bookId]});
            }else{
                res.json({"error" : "No order for this book"});
            }

        }else{
            res.json({"error" : "Invalid parameter bookId"});
        }
    }
}

module.exports = Order;