const request = require('request');
const md5 = require('md5');

class Books {
    async findBook(req, res){
        let params = req.query;
        if("keyword" in params){
            let url = "https://openlibrary.org/subjects/"+ encodeURI(params.keyword) +".json"

            let options = {json: true};

            try {
                request(url, options, async(error, result, body) => {
                    if (error) {
                        res.json(error);
                    }else{
                        let data = await new Books().formatResponse(body);
                        res.json({"status" : "Success", "data" : data});
                    }
                });
            }catch (e) {
                res.json({"error" : "No books with your keywords"});
            }

        }else{
            res.json({"error" : "Invalid parameter keyword"});
        }
    }

    async formatResponse(results){
        let finalResult = {};
        if("works" in results){
            let works = results.works;
            for await (let work of works){
                let bookId = md5(work.key);
                let objBook = {
                    "title" : work.title,
                    "authors" : work.authors.map(item => item.name),
                    "edition" : work.edition_count
                };
                finalResult[bookId] = objBook;
                global.books[bookId] = objBook;
            }
        }
        return finalResult;
    }
}

module.exports = Books;